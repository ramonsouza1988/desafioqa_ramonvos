package api;

import org.testng.Assert;

public class Validation {

	public static void validaResultado(String actual, String expected) {
		
		try {
			Assert.assertEquals(actual, expected);
			System.out.println("TEST PASSED: actual api text: "+actual + " | expected text: "+ expected );
		}catch (Throwable e) {

			Assert.fail("TEST FAILED" + e.getMessage());
		}
	}
	
}
