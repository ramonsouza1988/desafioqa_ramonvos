package api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Request {

	

	public void ApiRequest(String endpoint, String idFilme, String apiKey) throws IOException {

		URL url = new URL(endpoint.replace("[ID_DO_FILME]", idFilme).replace("[API_KEY]", apiKey));
		System.out.println("Endpoint: " +endpoint.replace("[ID_DO_FILME]", idFilme).replace("[API_KEY]", apiKey));
		System.out.println("Parametro Id Filme: "+idFilme);
		System.out.println("Parametro Api Key: "+apiKey);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		System.out.println("Method: GET");
		Constants.ApiResponseCode = con.getResponseCode();
		
		System.out.println("Status code: "+Constants.ApiResponseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		Constants.ApiResponse = content.toString();
		
		
		System.out.println("Response: "+Constants.ApiResponse);
		in.close();

		con.disconnect();
	}

}
