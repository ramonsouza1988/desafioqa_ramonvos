package api;

import com.google.gson.Gson;

public class DeserializeJson {

	public ApiDTO Deserialize() {

		
		ApiDTO obj = new Gson().fromJson(Constants.ApiResponse, ApiDTO.class);
		return obj;
	}
}
