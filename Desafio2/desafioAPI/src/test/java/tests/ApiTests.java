package tests;

import java.io.IOException;
import java.lang.reflect.Method;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import api.ApiDTO;
import api.Constants;
import api.DeserializeJson;
import api.Request;
import api.Validation;

public class ApiTests {

	
	@BeforeMethod
	public void before(Method method) {
		
		System.out.println();
		System.out.println("TestName: " +method.getName());
	}
	
	@Test
	public void TestApiBuscaDadosFilmeSucesso() throws IOException {
		
		
		String idFilme =Constants.idFilme;
		String apiKey = Constants.apiKey;
		String endpoint=Constants.endpoint.replace("[ID_DO_FILME]", idFilme).replace("[API_KEY]", apiKey);
		
		Request request = new  Request();
		request.ApiRequest(endpoint, idFilme, apiKey);
		
		DeserializeJson deserialize = new DeserializeJson();
		
		ApiDTO result = deserialize.Deserialize();
		
		String nomeFilme = "The Social Network";
		String anoFilme = "2010";
		String idiomaFilme = "English, French";
		
		Validation.validaResultado( result.getTitle(),nomeFilme);
		Validation.validaResultado(result.getYear(), anoFilme);
		Validation.validaResultado(result.getLanguage(), idiomaFilme);
		
	}
	
	
	@Test
	public void TestApiBuscaDadosFilmeInexistente() throws IOException {
		
		
		String idFilme ="aaajjjss";
		String apiKey = Constants.apiKey;
		String endpoint=Constants.endpoint.replace("[ID_DO_FILME]", idFilme).replace("[API_KEY]", apiKey);;
		
		Request request = new  Request();
		request.ApiRequest(endpoint, idFilme, apiKey);
		

		
		String incorretImdbId = "{\"Response\":\"False\",\"Error\":\"Incorrect IMDb ID.\"}";
		
		
		Validation.validaResultado( Constants.ApiResponse,incorretImdbId);
		
		
	}
}
