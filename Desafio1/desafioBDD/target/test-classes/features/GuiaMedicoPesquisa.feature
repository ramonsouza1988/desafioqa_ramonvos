#Author: your.email@your.domain.com
@features
Feature: Guia Medico
  	Para mostrar informações do guia de médicos Credenciados Unimed	
  Como usuário, desejo consultar por nome do médico e unidade de atendimento
  E visualizar as informações necessárias para escolher especialidade e entrar em contato

  @regressionTests
  Scenario: Pesquisar Medico Por Especialidade
    Given inicio browser e navego para a pagina principal
    Given clico em Guia Medico
    And informo a especialidade "Radiologia"
    And clico em Pesquisar
    And seleciono o estado "Rio de Janeiro" e cidade "Rio de Janeiro"
    And seleciono a rede de atendimento
    When e clico em continuar
    Then exibe resultados com a especialidade "Radiologia" na cidade do "Rio de Janeiro / RJ"

    
    @regressionTests
  Scenario: Pesquisar Medico Por Cidade
    Given inicio browser e navego para a pagina principal
    Given clico em Guia Medico
    And informo a especialidade "Radiologia"
    And clico em Pesquisar
    And seleciono o estado "Rio de Janeiro" e cidade "Rio de Janeiro"
    And seleciono a rede de atendimento
    When e clico em continuar
    Then nao exibe resultados  na cidade de "São Paulo / SP"