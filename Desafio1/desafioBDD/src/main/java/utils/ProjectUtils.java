package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import base.SeleniumBase;

public class ProjectUtils {

	public static void createFolder(String path) {

		try {

			// Create one directory
			boolean success = (new File(path)).mkdir();
			if (success) {
				System.out.println("Directory: " + path + " created");
			}

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static String getProjectPath() {

		return System.getProperty("user.dir");
	}

	public static void highlightElement(WebElement element) {

		JavascriptExecutor js = (JavascriptExecutor) SeleniumBase.driver;
		js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 2px solid red;");

	}

	public static Properties getProperties() {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(getProjectPath() + "\\src\\main\\java\\config\\configuration.properties");
			//
			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;

	}

}
