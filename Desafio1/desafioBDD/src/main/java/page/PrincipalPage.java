package page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.SeleniumBase;
import selenium.SeleniumMethods;

public class PrincipalPage {

	public PrincipalPage() {
		PageFactory.initElements(SeleniumBase.driver, this);
	}

	@FindBy(linkText = "Guia Médico")
	public WebElement linkGuiaMedicos;
	
	@FindBy(xpath = "div.form-overlay")
	public WebElement divLoading;
	
	
	
	public PrincipalPage abrirPaginaPrincipal() {
		SeleniumMethods.navigationToUrl("https://www.unimed.coop.br/");
		return this;
	}
	public PrincipalPage clicarMenuGuiaMedicos() {
		SeleniumMethods.clickElement(linkGuiaMedicos);
		return this;
	}
}
