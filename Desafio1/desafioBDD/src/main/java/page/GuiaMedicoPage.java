package page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import base.SeleniumBase;
import selenium.SeleniumMethods;

public class GuiaMedicoPage {

	public GuiaMedicoPage() {
		PageFactory.initElements(SeleniumBase.driver, this);
	}

	@FindBy(id = "campo_pesquisa")
	public WebElement txtPesquisa;

	@FindBy(id = "btn_pesquisar")
	public WebElement btnPesquisar;

	@FindBy(xpath = "(//*[@class='css-19bqh2r'])[1]")
	public WebElement btnSelect2Uf;

	@FindBy(xpath = "(//*[@class='css-19bqh2r'])[2]")
	public WebElement btnSelect2Cidade;

	@FindBy(id = "react-select-2-input")
	public WebElement txtUf;

	@FindBy(id = "react-select-2-option-18")
	public WebElement resultUf;

	@FindBy(id = "react-select-3-input")
	public WebElement txtCidade;

	@FindBy(id = "react-select-3-option-67")
	public WebElement resultCidade;

	@FindBy(className = "form-escolher-unimed-gm")
	public WebElement cssEscolherUnidade;

	@FindBy(xpath = "//button[@class='btn btn-success']")
	public WebElement btnContinuar;

	@FindBy(xpath = "//input[@type='radio']")
	public WebElement rbUnidade;

	@FindBy(id = "resultado0")
	public WebElement resultado;

	@FindBy(id = "txt_endereco")
	public WebElement labelEndereco;

	@FindBy(xpath = "//div[@class='container-resultado relative']")
	public WebElement cardResultado;

	@FindBy(id = "link_ver_tudo")
	public WebElement linkMaisDetalhes;

	public By locatorCardResult = By.xpath("//div[@class='container-resultado relative']");

	public By locatorLinkMais = By.xpath("//a[@id='link_ver_tudo']");

	public By locatorLinkMenosDetalhes = By.linkText(("Menos detalhes"));

	@FindBy(xpath =  "//a[contains(text(),'2')]")
	public WebElement btnPagina2;

	@FindBy(xpath =   "//a[contains(text(),'3')]")
	public WebElement btnPagina3;

	public GuiaMedicoPage preencherCampoPesquisa(String filtro) {

		SeleniumMethods.typeInTextBox(txtPesquisa, filtro);
		return this;
	}

	public GuiaMedicoPage preencherCampoUf(String uf) {

		SeleniumMethods.typeInTextBox(txtUf, uf);
		SeleniumMethods.clickElement(resultUf);

		return this;
	}

	public GuiaMedicoPage preencherCampoCidade(String cidade) {

		SeleniumMethods.typeInTextBox(txtCidade, cidade);
		SeleniumMethods.clickElement(resultCidade);
		return this;
	}

	public GuiaMedicoPage clicarBotaoPesquisar() {
		SeleniumMethods.clickElement(btnPesquisar);
		return this;
	}

	public GuiaMedicoPage clicarBotaoContinuar() {
		SeleniumMethods.clickElement(btnContinuar);
		return this;
	}

	public GuiaMedicoPage clicarBotaoPagina2() {
		SeleniumMethods.waitLoading();
		SeleniumMethods.clickElement(btnPagina2);
		SeleniumMethods.waitLoading();
		return this;
	}

	public GuiaMedicoPage clicarBotaoPagina3() {
		SeleniumMethods.waitLoading();
		SeleniumMethods.clickElement(btnPagina3);
		SeleniumMethods.waitLoading();
		return this;
	}

	public GuiaMedicoPage clicarRadioUnidade() {

		SeleniumMethods.waitElementVisible(cssEscolherUnidade);
		SeleniumMethods.clickElement(rbUnidade);
		return this;
	}

	public void validationResultNotExists(String cidade, int paginas) {

		for (int j = 1; j <= paginas; j++) {

			if(j ==1) {
				SeleniumMethods.waitElementVisible(resultado);
			}
			if(j == 2) {
				clicarBotaoPagina2();
			} else if(j == 3) {
				clicarBotaoPagina3();
			}
			
			

			List<WebElement> cards = SeleniumBase.driver.findElements(locatorCardResult);
			List<WebElement> links = SeleniumBase.driver.findElements(locatorLinkMais);

			for (int i = 0; i < cards.size(); i++) {

				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				SeleniumMethods.clickElement(links.get(i));
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				SeleniumMethods.waitElementVisible(cards.get(i));
				String conteudoCard = cards.get(i).getText().toUpperCase();
				System.out.println(conteudoCard);

				if (!conteudoCard.contains(cidade.toUpperCase())) {

					System.out.println("TEST PASSED - card " + i + " não contém a cidade de: " + cidade);
					Assert.assertTrue(true);

					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				else {

					System.out.println(
							"TEST FAILED - Card " + i + " contém a cidade que foi não foi pesquisada: " + cidade);

				}
			}

		}

	}

	public void validationResultExists(String especialidade, String cidade) {

		SeleniumMethods.waitElementVisible(resultado);

		List<WebElement> cards = SeleniumBase.driver.findElements(locatorCardResult);
		List<WebElement> links = SeleniumBase.driver.findElements(locatorLinkMais);

		for (int i = 0; i < cards.size(); i++) {

			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			SeleniumMethods.clickElement(links.get(i));
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			SeleniumMethods.waitElementVisible(cards.get(i));
			String conteudoCard = cards.get(i).getText().toUpperCase();
			System.out.println(conteudoCard);
			System.out.println(especialidade);
			if (conteudoCard.contains(especialidade.toUpperCase()) && conteudoCard.contains(cidade.toUpperCase())) {

				Assert.assertTrue(true);
				System.out.println("TEST PASSED - Card " + i + " Contém a Especialidade " + especialidade
						+ " e a Cidade: " + cidade);

				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (conteudoCard.contains(especialidade.toUpperCase())) {

				Assert.assertTrue(true);
				System.out.println("TEST PASSED - Card " + i + " Contém a Especialidade " + especialidade
						+ " em uma cidade visinha.");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			else {

				System.out.println("TEST FAILED - Card " + i + " não contém a especialidade esperada");
				Assert.assertTrue(false);

			}
		}

	}
}
