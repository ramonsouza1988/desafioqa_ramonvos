package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import gherkin.ast.Feature;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import logger.Reporter;
import utils.ProjectUtils;

public class SeleniumBase {

	public static WebDriver driver;

	@Before
	public void beforeClass() {

		driver = getChromeDriver();
		driver.manage().window().maximize();

		driver.manage().deleteAllCookies();

	}

	@After
	public void afterClass() {

		driver.quit();
	}

	private ChromeDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver",
				ProjectUtils.getProjectPath() + "\\src\\main\\java\\driver\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");

		return new ChromeDriver(options);

	}
}
