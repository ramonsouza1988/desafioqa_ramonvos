package logger;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentBDDReporter;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import utils.ProjectUtils;

public class Reporter {

	public static ExtentReports extent;
	public static ExtentTest test;
	public static String pathToReporter;

	public static void createReporter() {
		if (extent == null) {

			pathToReporter = ProjectUtils.getProjectPath() + "\\reporter\\ExtentReports.html";
			System.out.println(pathToReporter);
			ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(pathToReporter);

			// htmlReporter.config().setDocumentTitle("BDD tests");
			// htmlReporter.config().enableTimeline(true);
			// htmlReporter.config().setTimeStampFormat("dd/MM/yyyy HH:mm:ss");
			// htmlReporter.config().setEncoding("UTF-8");
			extent = new ExtentReports();

			extent.attachReporter(htmlReporter);
			extent.setAnalysisStrategy(AnalysisStrategy.BDD);

			extent.setSystemInfo("Browser", "Chrome");
			extent.setSystemInfo("BrowserVersion", "76");
			extent.setSystemInfo("OS", System.getProperty("os.name").toLowerCase());
			extent.setSystemInfo("Author", "ramon.souza1988@gmail.com");
			extent.setSystemInfo("User", System.getProperty("user.name").toLowerCase());
		}

	}

	public static void createScenario(String scenario) {

		test = extent.createTest(scenario);
	}

	public static void addStepsToReporter() {

	}

	public static void generateReporter() {
		extent.flush();
	}
}
