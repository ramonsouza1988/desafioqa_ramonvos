package selenium;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.SeleniumBase;
import utils.ProjectUtils;

public class SeleniumMethods {

	public static void clickElement(WebElement element) {
		waitElementVisible(element);

		element.click();
	}

	public static void typeInTextBox(WebElement element, String text) {

		waitElementVisible(element);
		element.clear();
		element.sendKeys(text);
		waitLoading();
		// element.sendKeys(Keys.TAB);
		waitLoading();
	}

	public static void navigationToUrl(String url) {

		SeleniumBase.driver.navigate().to(url);
	}

	public static boolean elementExists(WebElement element) {

		try {
			return element.isDisplayed();
		} catch (NoSuchElementException ex) {
			return false;
		}
	}

	public static void waitElementVisible(WebElement element) {
		try {

			WebDriverWait wait = new WebDriverWait(SeleniumBase.driver, 30);
			wait.until(ExpectedConditions.visibilityOf(element));
			ProjectUtils.highlightElement(element);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

	}

	public static void waitTextToBePresentInElement(WebElement element, String text) {
		try {

			WebDriverWait wait = new WebDriverWait(SeleniumBase.driver, 30);
			wait.until(ExpectedConditions.textToBePresentInElement(element, text));
			ProjectUtils.highlightElement(element);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

	}

	public static void waitElementClickable(By locator) {
		try {

			WebDriverWait wait = new WebDriverWait(SeleniumBase.driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(locator));

		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

	}

	public static void waitLoading() {

		for (int second = 0;; second++) {
			if (second >= 60)
				fail("timeout - waitForJavascripts");
			try {

				String documentState = ((JavascriptExecutor) SeleniumBase.driver)
						.executeScript("return document.readyState").toString();
				Object divLoad = ((JavascriptExecutor) SeleniumBase.driver)
						.executeScript("return document.querySelector('.form-overlay') == null");

				if (documentState.equals("complete") && divLoad.equals(true)) {
					break;
				}

			} catch (Throwable e) {
				System.out.println(e.getMessage());
			}

		}

	}
}
