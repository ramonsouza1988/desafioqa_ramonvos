package steps;

import java.lang.reflect.Method;
import java.util.Collection;

import gherkin.ast.Step;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import page.GuiaMedicoPage;
import page.PrincipalPage;

public class GuiaMedicoPesquisaSteps {

	public String scenarioName;
	public String scenarioSteps;

	

	@Given("inicio browser e navego para a pagina principal")
	public void inicio_browser_e_navego_para_a_pagina_principal() {

		new PrincipalPage().abrirPaginaPrincipal();
	}

	@Given("clico em Guia Medico")
	public void clico_em_Guia_Medico() {
		
		new PrincipalPage().clicarMenuGuiaMedicos();

	}

	@Given("informo a especialidade {string}")
	public void informo_a_especialidade(String string) {

		new GuiaMedicoPage().preencherCampoPesquisa(string);
	}

	@Given("clico em Pesquisar")
	public void clico_em_Pesquisar() {

		new GuiaMedicoPage().clicarBotaoPesquisar();
	}

	@Given("seleciono o estado {string} e cidade {string}")
	public void seleciono_o_estado_e_cidade(String string, String string2) {

		new GuiaMedicoPage().preencherCampoUf(string).preencherCampoCidade(string2);
	}

	@Given("seleciono a rede de atendimento")
	public void seleciono_a_rede_de_atendimento() {

		new GuiaMedicoPage().clicarRadioUnidade();
	}

	@When("e clico em continuar")
	public void e_clico_em_continuar() {

		new GuiaMedicoPage().clicarBotaoContinuar();
	}

	@Then("exibe resultados com a especialidade {string} na cidade do {string}")
	public void exibe_resultados_com_a_especialidade_na_cidade_do(String string, String string2) {

		new GuiaMedicoPage().validationResultExists(string,string2);
	}
	
	@Then("nao exibe resultados  na cidade de {string}")
	public void nao_exibe_resultados_na_cidade_de(String string) {
		
		new GuiaMedicoPage().validationResultNotExists(string,3);
	}

}
