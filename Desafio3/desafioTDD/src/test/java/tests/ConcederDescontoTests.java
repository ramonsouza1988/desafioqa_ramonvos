package tests;

import java.lang.reflect.Method;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ramonvos.desafioTDD.ConcederDesconto;
import ramonvos.desafioTDD.Validation;

public class ConcederDescontoTests {

	private int[] idFilmes = { 1, 2, 3, 4, 5, 6 };

	@BeforeClass
	public void beforeClass(){
		
		System.out.println("Critérios");
		System.out.println("Acima de R$ 100 e abaixo de R$ 200 => 10%");
		System.out.println("Acima de R$ 200 e abaixo de R$ 300 => 20%");
		System.out.println("Acima de R$ 300 e abaixo de R$ 400 => 25%");
		System.out.println("Acima de R$ 400 => 30%");
		System.out.println("Se existir no carrinho um filme com gênero ação somar + 5% de desconto");
		System.out.println();
	}
	
	@BeforeMethod
	public void beforeTest(Method method) {
		
		System.out.println();
		System.out.println("TestName: " +method.getName());
	}

	@Test
	public void TestaCompraSemDesconto() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[0];
		int qnt = 1;
		double valor = 45.00;
		double total = qnt * valor;
		double descontoEsperado = 0;
		double descontoAplicado = desconto.concedeDesconto(id, total);


		Validation.valuesAreEquals(id,qnt,valor,total,descontoEsperado,descontoAplicado);

	}

	@Test
	public void TestaCompraComDesconto10() {

		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[1];
		int qnt = 3;
		double valor = 55.00;
		double total = qnt * valor;
		double descontoEsperado = 10;
		double descontoAplicado = desconto.concedeDesconto(id, total);

		Validation.valuesAreEquals(id,qnt,valor,total,descontoEsperado,descontoAplicado);
	}

	@Test
	public void TestaCompraComDesconto20() {

		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[4];
		int qnt = 2;
		double valor = 100.00;
		double total = qnt * valor;
		double descontoEsperado = 20;
		double descontoAplicado = desconto.concedeDesconto(id, total);

		Validation.valuesAreEquals(id,qnt,valor,total,descontoEsperado,descontoAplicado);

	}

	@Test
	public void TestaCompraComDesconto30() {

		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[5];
		int qnt = 4;
		double valor = 100.00;
		double total = qnt * valor;
		double descontoEsperado = 30;
		double descontoAplicado = desconto.concedeDesconto(id, total);

		Validation.valuesAreEquals(id,qnt,valor,total,descontoEsperado,descontoAplicado);
	}

	@Test
	public void TestaCompraComDesconto35() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[2];
		int qnt = 4;
		double valor = 100.00;
		double total = qnt * valor;
		double descontoEsperado = 35;
		double descontoAplicado = desconto.concedeDesconto(id, total);

		Validation.valuesAreEquals(id,qnt,valor,total,descontoEsperado,descontoAplicado);
	}

	@Test
	public void TestaCompraSemDescontoValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[1];
		int qnt = 1;
		double valor1 = 0;
		double valor2 = 99.99;
		double descontoEsperado = 0;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		double descontoAplicado2 = desconto.concedeDesconto(id, valor2);

		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		Validation.valuesAreEquals(id,qnt,valor2,valor2,descontoEsperado,descontoAplicado2);

	}
	
	@Test
	public void TestaCompraComDesconto10ValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[1];
		int qnt = 1;
		double valor1 = 100.01;
		double valor2 = 100.99;
		double descontoEsperado = 10;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		double descontoAplicado2 = desconto.concedeDesconto(id, valor2);

		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		Validation.valuesAreEquals(id,qnt,valor2,valor2,descontoEsperado,descontoAplicado2);

	}
	
	@Test
	public void TestaCompraComDesconto20ValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[1];
		int qnt = 1;
		double valor1 = 200.01;
		double valor2 = 200.99;
		double descontoEsperado = 20;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		double descontoAplicado2 = desconto.concedeDesconto(id, valor2);

		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		Validation.valuesAreEquals(id,qnt,valor2,valor2,descontoEsperado,descontoAplicado2);

	}
	
	@Test
	public void TestaCompraComDesconto25ValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[1];
		int qnt = 1;
		double valor1 = 300.01;
		double valor2 = 300.99;
		double descontoEsperado = 25;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		double descontoAplicado2 = desconto.concedeDesconto(id, valor2);

		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		Validation.valuesAreEquals(id,qnt,valor2,valor2,descontoEsperado,descontoAplicado2);

	}
	
	@Test
	public void TestaCompraComDesconto30ValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[1];
		int qnt = 1;
		double valor1 = 400.01;
		
		double descontoEsperado = 30;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		

		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		

	}

	@Test
	public void TestaCompraComDesconto10EAdicionalValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[2];
		int qnt = 1;
		double valor1 = 100.01;
		
		double descontoEsperado = 10 + 5;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		
		
		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		
		
	}
	@Test
	public void TestaCompraComDesconto20EAdicionalValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[2];
		int qnt = 1;
		double valor1 = 200.01;
		
		double descontoEsperado = 20 + 5;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		
		
		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		
		
	}
	@Test
	public void TestaCompraComDesconto25EAdicionalValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[2];
		int qnt = 1;
		double valor1 = 300.01;
		
		double descontoEsperado = 25 + 5;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		
		
		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		
		
	}
	@Test
	public void TestaCompraComDesconto30EAdicionalValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[2];
		int qnt = 1;
		double valor1 = 400.01;
		
		double descontoEsperado = 30 + 5;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		
		
		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		
		
	}
	@Test
	public void TestaCompraSemDescontoComAdicionalValoresLimite() {
		ConcederDesconto desconto = new ConcederDesconto();
		int id = idFilmes[2];
		int qnt = 1;
		double valor1 = 99.99;
		
		double descontoEsperado = 0 + 5;
		double descontoAplicado1 = desconto.concedeDesconto(id, valor1);
		
		
		Validation.valuesAreEquals(id,qnt,valor1,valor1,descontoEsperado,descontoAplicado1);
		
		
	}
	

}
