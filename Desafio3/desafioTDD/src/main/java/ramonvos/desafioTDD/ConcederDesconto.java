package ramonvos.desafioTDD;

public class ConcederDesconto {

	public double concedeDesconto(int idFilme, double valor) {
		
		if (valor > 0) {
			double desconto = 0;
			if (valor >= 100 && valor < 200) {

				desconto = 10;
			} else if (valor >= 200 && valor < 300) {
				desconto = 20;
			} else if (valor >= 300 && valor < 400) {
				desconto = 25;
			} else if (valor >= 400) {
				desconto = 30;
			}

			if (idFilme == 3 || idFilme == 4) {
				desconto = desconto + 5;
			}
			return desconto;
		} else {
			return 0;
		}

	}

}
