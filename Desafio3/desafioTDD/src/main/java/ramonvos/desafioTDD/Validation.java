package ramonvos.desafioTDD;

import org.testng.Assert;

public class Validation {

	public static void valuesAreEquals(int id, int qnt, double valor, double total, double descontoEsperado,
			double descontoAplicado) {

		try {

			Assert.assertEquals(descontoEsperado, descontoAplicado);

			System.out.println("TEST PASSED - Filme id: " + id + " qnt: " + qnt + " valor: " + valor + " total: "
					+ total + " => desconto esperado: " + descontoEsperado + " - desconto aplicado: "
					+ descontoAplicado);
		} catch (Throwable e) {

			Assert.fail("TEST FAILED - Filme id: " + id + " qnt: " + qnt + " valor: " + valor + " total: " + total
					+ " => desconto esperado: " + descontoEsperado + " - desconto aplicado: " + descontoAplicado);
			System.out.println("TEST FAILED - Filme id: " + id + " qnt: " + qnt + " valor: " + valor + " total: "
					+ total + " => desconto esperado: " + descontoEsperado + " - desconto aplicado: "
					+ descontoAplicado);

		}

	}

}
